$(document).ready(function(){
    $('select').on('change', function () {
        var  formInputs = $('form').serialize();

        $.ajax({
            method: "GET",
            url: "/total-tires",
            data: {formInputs: formInputs}
        }).success(function (data) {
            $('#guma').show().children().show().html(' '+ data.total);
        });
    })
});