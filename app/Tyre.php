<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tyre extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pattern()
    {
        return $this->belongsTo(Pattern::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    /**
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeFilter($query, $filter)
    {
        if (isset($filter['height']) && $filter['height'] != null) {
            $query->where('height', $filter['height']);
        }

        if (isset($filter['width']) && $filter['width'] != null) {
            $query->where('width', $filter['width']);
        }

        if (isset($filter['radius']) && $filter['radius'] != null) {
            $query->where('radius', $filter['radius']);
        }

        if (isset($filter['manufacturer']) && $filter['manufacturer'] != null) {
            $query->whereHas('manufacturer', function ($query) use ($filter) {
                $query->where('id', $filter['manufacturer']);
            });
        }

        return $query;
    }

    public function scopeFetch($query, $column)
    {
        return $query->distinct()
            ->orderBy(\DB::raw('ABS('.$column.')'), 'desc')
            ->pluck($column);
    }

    /**
     * @param  array $request
     * @return mixed
     */
    public function seek($request)
    {
        return $this->filter($request)->where('active', 1);
    }

    /**
     * @param  $width
     * @param  string $height
     * @return string
     */
    public function displayDimensions($width, $height = '')
    {
        if ($height == '') {
            return $width;
        }

        return $width . '/' . $height;
    }

    /**
     * @return string
     */
    public function format()
    {
        if ($this->manufacturer->warranty == 1) {
            return '1 godina';
        } elseif ($this->manufacturer->warranty == 2) {
            return '2 godine';
        } elseif ($this->manufacturer->warranty == 3) {
            return '3 godine';
        }
    }

    /**
     * @return string
     */
    public function checkAvailability()
    {
        if ($this->availability == 'Proveri') {
            return "<span style='color: darkorange'>Nazovite za proveru stanja.</span>";
        } elseif ($this->availability == 'Ima') {
            return "<span style='color: forestgreen'>Ima na stanju.</span>";
        } elseif ($this->availability == 'Nema') {
            return "<span style='color: red'>Nema na stanju.</span>";
        }
    }

    /**
     * Cut down string at number word
     *
     * @param  $string
     * @param  $number
     * @return string
     */
    public function catTheCrap($string, $number)
    {
        $words = explode(' ', $string);

        if (count($words) > $number) {
            return implode(' ', array_slice($words, 0, $number)) . '...';
        }

        return $string;
    }

    /**
     * @return string
     */
    public function formatConstruction()
    {
        return $this->construction == 0 ? 'R' : '-';
    }

    /**
     * @return string
     */
    public function formatTubeType()
    {
        return $this->tube_type == '0' ? 'TL' : 'TT';
    }

    public function getSlug()
    {
        return $this->width.'-'.$this->height.'-'.$this->radius.'-'.$this->separate($this->manufacturer->name)
            .'-'.$this->separate($this->pattern->name).'-'.$this->getLsIndex($this->ls_index).'-'.$this->tubeType($this->tube_type);
    }

    protected function separate($words)
    {
        $wordArray = explode(' ', $words);
        return implode('-', $wordArray);
    }

    protected function tubeType($tubeType)
    {
        if ($tubeType == 1) {
            return 'TT';
        }
        return 'TL';
    }

    protected function getLsIndex($lsIndex)
    {
        $lsIndex = str_replace('/', '-', $lsIndex);
        $lsIndex = str_replace(' ', '-', $lsIndex);

        return $lsIndex;
    }
}
