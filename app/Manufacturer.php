<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tyres()
    {
        return $this->hasMany(Tyre::class);
    }

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'warranty',
        'logo_path',
        'description'
    ];
}
