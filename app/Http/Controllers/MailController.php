<?php

namespace App\Http\Controllers;

use App\Email;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MailController
 * @package App\Http\Controllers
 */
class MailController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $this->validate(request(), [
            'email' => 'required|email|unique:emails,email'
        ], [
            'email.required' => 'Email je obavezan!',
            'email.email' => 'Email nije dobar!',
            'email.unique' => 'Email vec postoji!',
        ]);

        try {
            Email::create(['email' => request('email')]);
        } catch (\Exception $e) {
            return response()->json(['email' => 'Doslo je do greske, molimo vas pokusajte kasnije'],
                Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['message' => 'Bravo!'], Response::HTTP_OK);
    }
}
