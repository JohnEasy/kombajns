<?php

namespace App\Http\Controllers\Email;

use App\Email;
use App\Http\Controllers\Controller;

/**
 * Class EmailController
 * @package App\Http\Controllers\Email
 */
class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.email.index');
    }

    /**
     * @param Email $email
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Email $email)
    {
        $email->delete();

        return back()->with('status', 'Bravo Jare');
    }
}
