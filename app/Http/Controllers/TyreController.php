<?php

namespace App\Http\Controllers;

use App\Manufacturer;
use App\Tyre;
use Illuminate\Http\Request;

class TyreController extends Controller
{
    /**
     * @var Tyre
     */
    protected $tyre;

    /**
     * TyreController constructor.
     * @param Tyre $tyre
     */
    public function __construct(Tyre $tyre)
    {
        $this->tyre = $tyre;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $widths = $this->tyre->fetch('width');
        $heights = $this->tyre->fetch('height');
        $radiuses = $this->tyre->fetch('radius');

        $heights = $this->discardNulableHeights($heights->toArray());
        $favourites = Tyre::where('favourite', 1)->orderBy(\DB::raw('ABS(price)'), 'asc')->get();

        $manufacturers = Manufacturer::orderBy('name', 'desc')->pluck('name', 'id');

        return view('tyres.index', compact('widths', 'heights', 'radiuses', 'manufacturers', 'favourites'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cene(Request $request)
    {
        $tyres = $this->tyre->seek($request->all())->orderBy(\DB::raw('ABS(price)'), 'asc')->paginate(10);

        $total = $tyres->total();

        $widths = $this->tyre->fetch('width');
        $heights = $this->tyre->fetch('height');
        $radiuses = $this->tyre->fetch('radius');

        $heights = $this->discardNulableHeights($heights->toArray());

        $manufacturers = Manufacturer::orderBy('name', 'desc')->pluck('name', 'id');
        return view('tyres.cene', compact('widths', 'heights', 'radiuses', 'manufacturers', 'tyres', 'total'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function totalTires()
    {
        parse_str(\request('formInputs'), $inputs);

        $total = $this->tyre->seek($inputs)->count();

        return response()->json(['total' => $total], 200);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $tyre = Tyre::where('slug', $slug)->first();

        return view('tyres.show', compact('tyre'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('tyres.contact');
    }

    /**
     * @param $heights
     * @return mixed
     */
    protected function discardNulableHeights(array $heights)
    {
        foreach ($heights as $key => $value) {
            if ($value == null) {
                unset($heights[$key]);
            }
        }
        return $heights;
    }
}
