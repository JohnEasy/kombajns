<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::latest()->paginate(3);

        return view('news.index', compact('news'));
    }

    public function show($title)
    {
        $article = News::where('title', $title)->first();
        $latestArticles = News::latest()->take(3)->get();


        return view('news.show', compact('article', 'latestArticles'));
    }
}
