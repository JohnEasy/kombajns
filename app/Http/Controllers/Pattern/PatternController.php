<?php

namespace App\Http\Controllers\Pattern;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePatternRequest;
use App\Http\Requests\UpdatePatternRequest;
use App\Pattern;
use Illuminate\Http\Request;

class PatternController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pattern.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pattern.pattern-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePatternRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePatternRequest $request)
    {
        $pattern = Pattern::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'image_path' => '',
        ]);

        $patternFile = $request->file('image');
        $patternFile->move(public_path() . '/images/patterns/' . $pattern->id, $patternFile->getClientOriginalName());
        $pattern->update([
            'image_path' => '/images/patterns/' . $pattern->id . '/' . $patternFile->getClientOriginalName()
        ]);

        return redirect('/admin/pattern/')->with('status', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pattern $pattern
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pattern $pattern)
    {
        return view('admin.pattern.pattern-edit', compact('pattern'));
    }

    /**
     *
     * Update the specified resource in storage.
     * @param Request $request
     * @param Pattern $pattern
     * @return Pattern
     */
    public function update(UpdatePatternRequest $request, Pattern $pattern)
    {
        $pattern->update([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        if ($request->hasFile('image')) {
            \File::deleteDirectory(public_path('images/patterns/' . $pattern->id));
            $patternFile = $request->file('image');
            $patternFile->move(public_path() . '/images/patterns/' . $pattern->id, $patternFile->getClientOriginalName());
            $pattern->update([
                'image_path' => '/images/patterns/' . $pattern->id . '/' . $patternFile->getClientOriginalName()
            ]);
        }

        return redirect('/admin/pattern/')->with('status', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Pattern $pattern
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pattern $pattern)
    {
        \File::deleteDirectory(public_path('images/patterns/' . $pattern->id));
        $pattern->delete();

        return back()->with('status', 'Successfully deleted');
    }
}
