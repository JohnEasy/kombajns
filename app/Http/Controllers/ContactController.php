<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Manufacturer;
use App\Tyre;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.contact');
    }

    public function store()
    {
        $this->validate(\request(), [
            'email' => 'required|email|max:100',
            'phone' => ['required', 'regex:/^([^<>\'])+$/'],
            'content' => ['required', 'regex:/^([^<>\'])+$/']
        ], [
            'email.required' => 'Email je obavezan',
            'phone.required' => 'Telefon je obavezan',
            'content.required' => 'Kontent je obavezan',
        ]);

        Contact::create(\request()->except('_token'));


        return back()->with('status', 'Uspesno prosledjena poruka');
    }
}
