<?php

namespace App\Http\Controllers\Manufacturer;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateManufacturerStoreRequest;
use App\Http\Requests\UpdateMannufacturerRequest;
use App\Manufacturer;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.manufacturer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manufacturer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateManufacturerStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateManufacturerStoreRequest $request)
    {
        $manufacturer = Manufacturer::create([
            'name' => $request->get('name'),
            'warranty' => $request->get('warranty'),
            'logo_path' => '',
            'description' => $request->get('description'),

        ]);

        $logo = $request->file('logo');
        $logo->move(public_path() . '/images/manufacturers/' . $manufacturer->id, $logo->getClientOriginalName());
        $manufacturer->update(['logo_path' => '/images/manufacturers/' . $manufacturer->id . '/' . $logo->getClientOriginalName()]);

        return redirect('/admin/manufacturer')->with('status', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Manufacturer $manufacturer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Manufacturer $manufacturer)
    {
        return view('admin.manufacturer.edit', compact('manufacturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMannufacturerRequest $request
     * @param Manufacturer $manufacturer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateMannufacturerRequest $request, Manufacturer $manufacturer)
    {
        $manufacturer->update([
            'name' => $request->get('name'),
            'warranty' => $request->get('warranty'),
            'description' => $request->get('description'),
        ]);

        if ($request->hasFile('logo')) {
            \File::deleteDirectory(public_path('images/manufacturers/' . $manufacturer->id));
            $logo = $request->file('logo');
            $logo->move(public_path() . '/images/manufacturers/' . $manufacturer->id, $logo->getClientOriginalName());
            $manufacturer->update(['logo_path' => '/images/manufacturers/' . $manufacturer->id . '/' . $logo->getClientOriginalName()]);
        }
        return redirect('/admin/manufacturer')->with('status', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Manufacturer $manufacturer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Manufacturer $manufacturer)
    {
        \File::deleteDirectory(public_path('images/manufacturers/' . $manufacturer->id));
        $manufacturer->delete();

        return back()->with('status', 'Successfully deleted');
    }
}
