<?php

namespace App\Http\Controllers\Tyre;

use App\Http\Requests\CreateTyreStoreRequest;
use App\Manufacturer;
use App\Pattern;
use App\Tyre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class TyreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tyre.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturer::get(['id', 'name']);
        $patterns = Pattern::get(['id', 'name']);

        return view('admin.tyre.create', compact('manufacturers', 'patterns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTyreStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTyreStoreRequest $request)
    {
        $tyre = Tyre::create($request->except('_token'));
        $tyre->update(['slug' => $tyre->getSlug()]);

        return redirect('admin/tyre')->with('status', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tyre $tyre
     * @return \Illuminate\Http\Response
     */
    public function edit(Tyre $tyre)
    {
        $manufacturers = Manufacturer::get(['id', 'name']);
        $patterns = Pattern::get(['id', 'name']);

        return view('admin.tyre.edit', compact('tyre', 'manufacturers', 'patterns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Tyre $tyre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tyre $tyre)
    {
        $tyre->update($request->except('_token'));
        $tyre->update(['slug' => $tyre->getSlug()]);

        return redirect('admin/tyre')->with('status', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tyre $tyre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tyre $tyre)
    {
        $tyre->delete();

        return back()->with('success', 'Successfully deleted');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePrice()
    {
        try {
            Tyre::findOrFail(request('tyreId'))->update(['price' => request('price')]);
            return response()->json(['success'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['error'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
