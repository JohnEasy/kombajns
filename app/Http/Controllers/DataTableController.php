<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Email;
use App\Manufacturer;
use App\News;
use App\Pattern;
use App\Tyre;
use Yajra\Datatables\Datatables;

class DataTableController extends Controller
{
    /**
     * @return mixed
     */
    public function manufacturers()
    {
        $manufacturers = Manufacturer::select(['id', 'name', 'warranty']);

        return Datatables::of($manufacturers)
            ->addColumn('edit', function ($manufacturer) {
                return '<a href="/admin/manufacturer/' . $manufacturer->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->addColumn('delete', function ($manufacturer) {
                return '<a href="/admin/manufacturer/' . $manufacturer->id . '/delete" > Delete</a>';
            })
            ->make(true);
    }

    /**
     * @return mixed
     */
    public function patterns()
    {
        $patterns = Pattern::select(['id', 'name', 'description']);

        return Datatables::of($patterns)
            ->addColumn('edit', function ($pattern) {
                return '<a href="/admin/pattern/' . $pattern->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })->addColumn('delete', function ($pattern) {
                return '<a href="/admin/pattern/' . $pattern->id . '/delete"> Delete</a>';
            })->make(true);
    }

    public function tyres()
    {
        $tyres = Tyre::with('manufacturer', 'pattern')->get();

        return Datatables::of($tyres)
            ->addColumn('edit', function ($tyre) {
                return '<a href="/admin/tyre/' . $tyre->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })->addColumn('delete', function ($tyre) {
                return '<a href="/admin/tyre/' . $tyre->id . '/delete" onclick="confirm(\'sure?\')"> Delete</a>';
            })->addColumn('price', function ($tyre) {
                return '<input type=number data-id="'.$tyre->id.'" value="'.$tyre->price.'" onchange="processPrice(this)">';
            })->make(true);
    }

    public function news()
    {
        $news = News::select(['id', 'title']);

        return Datatables::of($news)
            ->addColumn('edit', function ($n) {
                return '<a href="/admin/news/' . $n->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })->addColumn('delete', function ($n) {
                return '<a href="/admin/news/' . $n->id . '/delete"> Delete</a>';
            })->make(true);
    }

    public function emails()
    {
        $emails = Email::all();

        return Datatables::of($emails)->addColumn('delete', function ($e) {
            return '<a href="/admin/emails/' . $e->id . '/delete"> Delete</a>';
        })->make(true);
    }

    public function contacts()
    {
        $contacts = Contact::all();

        return Datatables::of($contacts)->addColumn('delete', function ($e) {
            return '<a href="#"> Delete</a>';
        })->make(true);
    }
}
