<?php

namespace App\Services;


class Helpers
{
    /**
     * Cut down string at number word
     *
     * @param  $string
     * @param  $number
     * @return string
     */
    public function catTheCrap($string, $number)
    {
        $words = explode(' ', $string);

        if (count($words) > $number) {
            return implode(' ', array_slice($words, 0, $number)) . '...';
        }

        return $string;
    }
}