<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'emails';

    protected $fillable = ['email'];
}

//CREATE TABLE `kamijoni`.`emails` (
//`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//  `email` VARCHAR(45) NOT NULL,
//  `created_at` TIMESTAMP NULL DEFAULT NULL,
//  `updated_at` TIMESTAMP NULL DEFAULT NULL,
//  PRIMARY KEY (`id`));
