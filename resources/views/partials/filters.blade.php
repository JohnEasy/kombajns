<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<div class="row" style="margin-bottom: 20px">
    <div class="col-lg-12 text-center">
        <form class="form-inline" method="get" action="/#results">
            <div class="form-group">
                <select name="width" class="form-control" title="width">
                    <option selected value="">Širina</option>
                    @foreach($widths as $width)
                        <option value="{{$width}}" {{Request::get('width') == $width ? 'selected' : ''}}>{{$width}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select name="height" class="form-control" title="height">
                    <option selected value="">Visina</option>
                    @foreach($heights as $height)
                        <option value="{{$height}}" {{Request::get('height') == $height ? 'selected' : ''}}>{{$height}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <select name="radius" class="form-control" title="radius">
                    <option selected value="">Prečnik</option>
                    @foreach($radiuses as $radius)
                        <option value="{{$radius}}" {{Request::get('radius') == $radius ? 'selected' : ''}}>{{$radius}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <select name="manufacturer" class="form-control">
                    <option selected value="">Prozvođač</option>
                    @foreach($manufacturers as $manufacturerId => $manufacturer)
                        <option value="{{$manufacturerId}}" {{Request::get('manufacturer') == $manufacturerId ? 'selected' : ''}}>{{$manufacturer}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-success">Pronađi gumu</button>
        </form>
    </div>
</div>
