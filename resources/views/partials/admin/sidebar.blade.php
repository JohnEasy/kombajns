<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="active">
                <a class="" href="#">
                    <i class="icon_house_alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="/admin/tyre" class="">Tyre List</a>
            </li>
            <li class="sub-menu">
                <a href="/admin/manufacturer/" class="">List Manufacturers</a>
            </li>

            <li class="sub-menu">
                <a href="/admin/pattern" class="">List Patterns</a>
            </li>

            <li class="sub-menu">
                <a href="/admin/news" class="">List News</a>
            </li>

            <li class="sub-menu">
                <a href="/admin/emails" class="">Emails</a>
            </li>

            <li class="sub-menu">
                <a href="/admin/contacts" class="">Contact form</a>

            <li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>