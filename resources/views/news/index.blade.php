@extends('layouts.main.master')
@inject('helper', 'App\Services\Helpers')

@section('content')
<div id="wrap" class="color1-inher">
    <!-- Header -->
    <div id="wrap-body" class="p-t-lg-30">
        <div class="container">
            <div class="wrap-body-inner">
                <!-- Breadcrumb-->
                <div class="hidden-xs">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="ht-breadcrumb pull-left">
                                <li class="home-act"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Novosti</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Blog -->
                <section class="blog-page m-t-lg-30 m-t-xs-0 p-b-lg-45">
                    <div class="">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-10">
                                <!-- Blog item -->
                                <div class="blog blog-grid blog-lg">
                                    @foreach($news as $new)
                                        <div class="blog-item">
                                            {{--<a href="#" class="hover-img"><img src="images/default3.png" alt="image"></a>--}}
                                            <div class="blog-caption text-left">
                                                <ul class="blog-date blog-date-left">
                                                    <li><i class="fa fa-calendar"></i> {{$new->created_at->toFormattedDateString()}}</li>
                                                </ul>
                                                <h2 class="blog-heading"><a href="#">{{$new->title}}</a></h2>
                                                <p>{!! $helper->catTheCrap($new->body, 100)!!}</p>
                                                <a href="{{url('novosti/' . $new->title)}}" class="ht-btn ht-btn-default ">Pročitaj više</a>
                                            </div>
                                        </div>
                                    @endforeach

                                        {{$news->links('vendor.pagination.default')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection