@extends('layouts.main.master')

@section('content')
<div id="wrap-body" class="p-t-lg-30">
    <div class="container">
        <div class="wrap-body-inner">
            <!-- Breadcrumb-->
            <div class="hidden-xs">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ht-breadcrumb pull-left">
                            <li class="home-act"><a href="#"><i class="fa fa-home"></i></a></li>
                            <li class="home-act"><a href="#">Vesti</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Blog details -->
            <section class="blog-detail-page m-t-lg-30 m-t-xs-0 p-b-lg-45 ">
                <div class="">
                    <div class="row">
                        <div class="col-sm-7 col-md-8 col-lg-8">
                            <div class="blog blog-grid blog-lg">
                                <!-- Blog item -->
                                <div class="blog-item  no-bg p-lg-0">
                                    <div class="blog-caption text-left">
                                        <ul class="blog-date blog-date-left">
                                            <li><a href="#"><i class="fa fa-calendar"></i>{{$article->updated_at->toFormattedDateString()}}</a></li>
                                        </ul>
                                        <h2 class="blog-heading">{{$article->title}}</h2>
                                        <p>{!! $article->body!!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Blog category -->
                        <div class="col-sm-5 col-md-4 col-lg-4">
                            <!-- Last posts -->
                            <div class="blog blog-list blog-sm m-b-lg-30">
                                <div class="heading-1">
                                    <h3>Najnoviji postovi</h3>
                                </div>
                                @foreach($latestArticles as $latestArticle)
                                    <div class="blog-item">
                                        <div class="row">
                                            <div class="col-sm-4 col-md-4 p-r-lg-0">
                                            </div>
                                            <div class="col-sm-8 col-md-8 p-r-lg-0">
                                                <ul class="blog-date blog-date-left">
                                                    <li><a href="#"><i class="fa fa-calendar"></i>{{$latestArticle->updated_at->toFormattedDateString()}}</a></li>
                                                </ul>
                                                <div class="blog-caption">
                                                    <h4 class="heading-3 blog-heading"><a href="{{url('/novosti/' . $latestArticle->title)}}">{{$latestArticle->title}}</a></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>
@endsection
