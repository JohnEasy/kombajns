@extends('layouts.main.master')

@section('content')
    <div id="wrap-body" class="p-t-lg-30">
        <div class="container">
            <div class="wrap-body-inner">
                <!-- Breadcrumb-->
                <div class="hidden-xs">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="ht-breadcrumb pull-left">
                                <li class="home-act"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Kako kupiti?</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Contact -->
                <section class="block-contact m-t-lg-30 m-t-xs-0 p-b-lg-50">
                    <div class="">
                        <div class="row">
                            <!-- Contact info -->
                            <div class="col-sm-6 col-md-6 col-lg-6 m-b-xs-50">
                                <div class="heading">
                                    <h3>Kako kupiti?</h3>
                                </div>
                                <div class="contact-info p-lg-30 p-xs-15 bg-gray-fa bg1-gray-2">
                                    <div class="content">
                                        <p>
                                            <strong>Odabir gume:</strong>
                                        </p>

                                        <p>
                                            Odaberite gumu koristeći filtere u pretraživaču guma. Za informacije i naručivanje nas nazovite ili pošaljite upit mejlom. Ukoliko niste na sajtu pronašli gumu koju ste tražili - slobodno nas kontaktirajte. Zbog širokog asortimana koji se često dopunjuje i menja, na sajtu se ne nalaze sve gume koje možemo ponuditi.

                                        </p>
                                        <p><strong>Način plaćanja:</strong></p>
                                        <p>

                                            Nakon odabira gume i potvrde da su gume raspoložive, poslaćemo Vam predračun elektronski (mejl, viber, sms) koji možete uplatiti elektronskim putem (e-bankingom) ili uplatnicom u banci/pošti. Nakon evidentiranja uplate, izdajemo nalog za isporuku guma.
                                            

                                        </p>

                                        <p><strong>Isporuka guma:</strong></p>

                                        <p>
                                            Gume najčešće isporučujemo kurirskom službom na adresu koju Vi želite, ili našim vozilom, u zavisnosti od broja komada i Vaše lokacije.
                                            Cene isporuke kurirskim službama su veoma povoljne (imamo dugogodišnje ugovore za slanje guma sa nekoliko kurirskih službi) i često je ovakva isporuka najjednostavnije i najbrže rešenje.
                                            Sa našom prodajnom službom uvek možete usaglasiti isporuku i dogovoriti sve detalje.
                                        </p>

                                        <p><strong>
                                            Još jednom Vas pozivamo da nas slobodno kontaktirate oko svih upita, pitanja i dodatnih informacija. Možemo Vam dati preporuke oko odabira guma, preneti iskustva drugih prevoznika i zajedno pronaći pravu gumu za Vaš posao!</strong>
                                        </p>
                                        <p class="text-info">
                                            *Zbog širokog asortimana i velikog broja artikala, moguce su greške u opisu proizvoda, kao i cenama. Molimo Vas da nam se obratite ukoliko primetite neka neslaganja u opisima.
                                        </p>

                                        <ul class="list-default">
                                            <li><i class="fa fa-clock-o"></i>Beograd, Srbija</li>
                                            <li><i class="fa fa-phone"></i>062 404 222</li>
                                            <li><i class="fa fa-envelope-o"></i>viragrar@gmail.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Contact form -->
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="heading">
                                    <h3>Pošaljite nam upit</h3>
                                </div>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (session('status'))
                                    <div class="alert alert-info">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <div class="contact-form p-lg-30 p-xs-15 bg-gray-fa bg1-gray-2">
                                    <form action="{{url('instructions')}}" method="POST">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{old('email')}}" class="form-control form-item" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="phone" value="{{old('phone')}}" class="form-control form-item" placeholder="Telefon">
                                        </div>
                                        <textarea name="content" class="form-control form-item h-200 m-b-lg-10" placeholder="Poruka" rows="3">{{old('content')}}</textarea>
                                        <button type="submit" class="ht-btn ht-btn-default">Pošalji</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection