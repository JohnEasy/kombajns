@extends('layouts.admin.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Manufacturer</div>
                    <div class="panel-body">
                        <form class="form-horizontal"
                              role="form"
                              method="POST"
                              enctype="multipart/form-data"
                              action="{{ '/admin/manufacturer/'. $manufacturer->id .'/update' }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $manufacturer->name }}" autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('warranty') ? ' has-error' : '' }}">
                                <label for="warranty" class="col-md-4 control-label">Warranty</label>

                                <div class="col-md-6">
                                    <select id="warranty" class="form-control" name="warranty" >
                                        <option selected>Select</option>
                                        <option {{$manufacturer->warranty == 1 ? 'selected' : ''}}>1</option>
                                        <option {{$manufacturer->warranty == 2 ? 'selected' : ''}}>2</option>
                                        <option {{$manufacturer->warranty == 3 ? 'selected' : ''}}>3</option>
                                    </select>

                                    @if ($errors->has('warranty'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('warranty') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label for="logo" class="col-md-4 control-label">Logo</label>

                                <div class="col-md-6">
                                    <input type="file" name="logo">

                                    @if ($errors->has('logo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">description</label>

                                <div class="col-md-6">
                                    <textarea cols="75" rows=10" name="description">{{$manufacturer->description}}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    <img src="{{asset($manufacturer->logo_path)}}" alt="logo-manufacturer">
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
