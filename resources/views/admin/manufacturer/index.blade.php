@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Manufacturers</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="/admin/manufacturer/create" class="btn btn-primary pull-left" >Create new manufacturer</a>
        </div>
    </div>

    <table class="table table-bordered" id="manufacturers-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Warranty</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#manufacturers-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-manufacturers/',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'warranty', name: 'warranty'},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    </script>
@endsection
