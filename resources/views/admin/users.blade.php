@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Users</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="/admin/user/create" class="btn btn-primary pull-left" >Create new user</a>
        </div>
    </div>

    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Last login</th>

            <th>Activate</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>

    <script>
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/get-users',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},

                {data: 'last_login', name: 'last_login'},
                {data: 'activated', name: 'activated', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            ]
        });

        function activateUser(userId) {
            var element = $('#user-' + userId);

            $.ajax({
                method: "GET",
                url: "/admin/activate-user",
                data: {id: userId}
            })
            .success(function (user) {
                if (user.active) {
                    return element
                        .removeClass('btn-warning')
                        .addClass('btn-primary')
                        .html('<i class="glyphicon glyphicon-collapse-down"></i> Deactivate');
                }
                element
                    .removeClass('btn-primary')
                    .addClass('btn-warning')
                    .html('<i class="glyphicon glyphicon-collapse-up"></i> Activate');
            });
        }

        function deleteUser(userId) {
            var result = confirm("Want to delete?");
            var element = $('#delete-' + userId);

            if (result) {
                $.ajax({
                    method: "GET",
                    url: "/admin/delete-user",
                    data: {id: userId}
                }).success(function (data) {
                    if (data.status) {
                        element.closest('tr').remove();
                    }
                });
            }
        }
    </script>
@stop