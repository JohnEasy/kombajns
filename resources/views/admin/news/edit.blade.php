@extends('layouts.admin.master')

@section('content')
        <!DOCTYPE html>
<html>
<head>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea',
            entity_encoding : "named",
            image_dimensions: false,
            image_class_list: [
                {title: 'Responsive', value: 'img-responsive'}
            ],
            plugins: [
                'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'save table contextmenu directionality emoticons template paste textcolor responsiveImage'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',

        });

    </script>
</head>
<body>

<form action="{{ '/admin/news/'.$news->id.'/update' }}" method="post">
    {{csrf_field()}}
    <input name="title" value="{{$news->title}}">
    <textarea name="body" rows="40">{{$news->body}}</textarea>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

</body>
</html>
@endsection