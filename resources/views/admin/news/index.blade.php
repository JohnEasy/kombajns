@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> News</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="/admin/news/create" class="btn btn-primary pull-left" >Create news</a>
        </div>
    </div>

    <table class="table table-bordered" id="manufacturers-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#manufacturers-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-news/',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    </script>
@endsection
