@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Tyres</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="/admin/tyre/create" class="btn btn-primary pull-left" >Create new tyre</a>
        </div>
    </div>

    <table class="table table-bordered" id="tyre-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>W</th>
            <th>H</th>
            <th>Const</th>
            <th>R</th>
            <th>Man</th>
            <th>Pattern</th>
            <th>Price</th>
            <th>LsIndex</th>
            <th>TubeType</th>
            <th>Type</th>
            <th>Action</th>
            <th>Availability</th>
            <th>Active</th>
            <th>Updated at</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#tyre-table').DataTable({
            iDisplayLength: 100,
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-tyres/',
            columns: [
                {data: 'id', name: 'tyres.id'},
                {data: 'width', name: 'width'},
                {data: 'height', name: 'height'},
                {data: 'construction', name: 'construction'},
                {data: 'radius', name: 'radius'},
                {data: 'manufacturer.name', name: 'manufacturer.name'},
                {data: 'pattern.name', name: 'pattern.name'},
                {data: 'price', name: 'price'},
                {data: 'ls_index', name: 'ls_index'},
                {data: 'tube_type', name: 'tube_type'},
                {data: 'type', name: 'type'},
                {data: 'action', name: 'action'},
                {data: 'availability', name: 'availability'},
                {data: 'active', name: 'active'},
                {data: 'updated_at', name: 'updated_at'},

                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });

        function processPrice(element) {
            var price = $(element).val();
            var tyreId = $(element).data("id");

            var request = $.ajax({
               url: "{{route('updatePrice')}}",
               data: {price: price, tyreId: tyreId},
               dataType: 'json'
            });

            request.done(function( msg ) {
                $(element).css('border-color', 'green');
                $(element).css('color', 'green');
                setTimeout(function () {
                    $(element).css('border-color', '');
                    $(element).css('color', '');
                }, 1000);
            });

            request.fail(function( jqXHR, textStatus ) {
                $(element).css('border-color', 'red');
            });
        }
    </script>
@endsection
