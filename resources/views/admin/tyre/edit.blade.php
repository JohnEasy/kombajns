@extends('layouts.admin.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Tyre</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{'/admin/tyre/' . $tyre->id . '/update'}}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('width') ? ' has-error' : '' }}">
                                <label for="width" class="col-md-4 control-label">Width</label>

                                <div class="col-md-6">
                                    <input id="width" type="text" class="form-control" name="width" value="{{ $tyre->width }}" required autofocus>

                                    @if ($errors->has('width'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('width') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
                                <label for="height" class="col-md-4 control-label">Height</label>

                                <div class="col-md-6">
                                    <input id="height" type="text" class="form-control" name="height" value="{{ $tyre->height }}">

                                    @if ($errors->has('height'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('construction') ? ' has-error' : '' }}">
                                <label for="construction" class="col-md-4 control-label">Construction</label>

                                <div class="col-md-6">

                                    <select id="construction" class="form-control" name="construction" required>
                                        <option value="1" {{$tyre->construction == 1 ? 'selected' : '' }}>-</option>
                                        <option value="0" {{$tyre->construction == 0 ? 'selected' : '' }}>R</option>
                                    </select>

                                    @if ($errors->has('construction'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('construction') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('radius') ? ' has-error' : '' }}">
                                <label for="radius" class="col-md-4 control-label">Radius</label>

                                <div class="col-md-6">
                                    <input id="radius" type="text" value="{{$tyre->radius}}" class="form-control" name="radius" required>

                                    @if ($errors->has('radius'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('radius') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manufacturer') ? ' has-error' : '' }}">
                                <label for="manufacturer" class="col-md-4 control-label">Manufacturer</label>

                                <div class="col-md-6">
                                    <select id="manufacturer" class="form-control" name="manufacturer_id" required>
                                        @foreach($manufacturers as $manufacturer)
                                            <option value="{{$manufacturer->id}}" {{$tyre->manufacturer->id == $manufacturer->id ? 'selected' : ''}}>
                                                {{$manufacturer->name}}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('manufacturer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('manufacturer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pattern') ? ' has-error' : '' }}">
                                <label for="pattern" class="col-md-4 control-label">Pattern</label>

                                <div class="col-md-6">
                                    <select id="pattern" class="form-control" name="pattern_id" required>
                                        @foreach($patterns as $pattern)
                                            <option value="{{$pattern->id}}" {{$tyre->pattern->id == $pattern->id ? 'selected' : ''}}>
                                                {{$pattern->name}}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('pattern'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pattern') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ls_index') ? ' has-error' : '' }}">
                                <label for="ls_index" class="col-md-4 control-label">Ls index</label>

                                <div class="col-md-6">
                                    <input id="ls_index" type="text" class="form-control" name="ls_index" value="{{ $tyre->ls_index }}">

                                    @if ($errors->has('ls_index'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ls_index') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tube_type') ? ' has-error' : '' }}">
                                <label for="tube_type" class="col-md-4 control-label">Tube type</label>

                                <div class="col-md-6">
                                    <select id="tube_type" class="form-control" name="tube_type" required>
                                        <option value="0" {{$tyre->tube_type == 0 ? 'selected' : ''}}>TL</option>
                                        <option value="1" {{$tyre->tube_type == 1 ? 'selected' : ''}}>TT</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type" class="col-md-4 control-label">Type</label>

                                <div class="col-md-6">
                                    <select id="type" class="form-control" name="type" required>
                                        <option {{$tyre->type == 'Spoljna' ? 'selected' : ''}}>Spoljna</option>
                                        <option {{$tyre->type == 'Protektirana' ? 'selected' : ''}}>Protektirana</option>
                                        <option {{$tyre->type == 'Komplet' ? 'selected' : ''}}>Komplet</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('action') ? ' has-error' : '' }}">
                                <label for="action" class="col-md-4 control-label">Action</label>

                                <div class="col-md-6">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="action" name="action" value="1" {{$tyre->action == 1 ? 'checked' : ''}}>Yes
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="action" name="action" value="0" {{$tyre->action == 0 ? 'checked' : ''}}>No
                                        </label>
                                    </div>

                                    @if ($errors->has('action'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('action') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('availability') ? ' has-error' : '' }}">
                                <label for="availability" class="col-md-4 control-label">Availability</label>

                                <div class="col-md-6">
                                    <select id="availability" class="form-control" name="availability" required>
                                        <option {{$tyre->availability == 'Ima' ? 'selected' : ''}}>Ima</option>
                                        <option {{$tyre->availability == 'Nema' ? 'selected' : ''}}>Nema</option>
                                        <option {{$tyre->availability == 'Proveri' ? 'selected' : ''}}>Proveri</option>
                                    </select>

                                    @if ($errors->has('availability'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('availability') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label for="active" class="col-md-4 control-label">Active</label>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="" name="active" value="1" {{$tyre->active == 1 ? 'checked' : ''}}>Yes
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="" name="active" value="0" {{$tyre->active == 0 ? 'checked' : ''}}>No
                                        </label>
                                    </div>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Price</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="{{$tyre->price}}" name="price" id="price">

                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fav') ? ' has-error' : '' }}">
                                <label for="fav" class="col-md-4 control-label">Fav</label>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="" name="favourite" value="1" {{$tyre->favourite == 1 ? 'checked' : ''}}>Yes
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="" name="favourite" value="0" {{$tyre->favourite == 0 ? 'checked' : ''}}>No
                                        </label>
                                    </div>

                                    @if ($errors->has('fav'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
