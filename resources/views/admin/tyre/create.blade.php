@extends('layouts.admin.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Tyre</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ '/admin/tyre/create' }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('width') ? ' has-error' : '' }}">
                                <label for="width" class="col-md-4 control-label">Width</label>

                                <div class="col-md-6">
                                    <input id="width" type="text" class="form-control" name="width" value="{{ old('width') }}" required autofocus>

                                    @if ($errors->has('width'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('width') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
                                <label for="height" class="col-md-4 control-label">Height</label>

                                <div class="col-md-6">
                                    <input id="height" type="text" class="form-control" name="height" value="{{ old('height') }}">

                                    @if ($errors->has('height'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('construction') ? ' has-error' : '' }}">
                                <label for="construction" class="col-md-4 control-label">Construction</label>

                                <div class="col-md-6">

                                    <select id="construction" class="form-control" name="construction" required>
                                        <option value="1">R</option>
                                        <option value="0">-</option>
                                    </select>

                                    @if ($errors->has('construction'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('construction') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('radius') ? ' has-error' : '' }}">
                                <label for="radius" class="col-md-4 control-label">Radius</label>

                                <div class="col-md-6">
                                    <input id="radius" type="text" class="form-control" name="radius" required>

                                    @if ($errors->has('radius'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('radius') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manufacturer') ? ' has-error' : '' }}">
                                <label for="manufacturer" class="col-md-4 control-label">Manufacturer</label>

                                <div class="col-md-6">
                                    <select id="manufacturer" class="form-control" name="manufacturer_id" required>
                                        <option>Choose..</option>
                                        @foreach($manufacturers as $manufacturer)
                                            <option value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('manufacturer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('manufacturer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pattern') ? ' has-error' : '' }}">
                                <label for="pattern" class="col-md-4 control-label">Pattern</label>

                                <div class="col-md-6">
                                    <select id="pattern" class="form-control" name="pattern_id" required>
                                        <option>Choose..</option>
                                        @foreach($patterns as $pattern)
                                                <option value="{{$pattern->id}}">{{$pattern->name}}</option>
                                            @endforeach
                                    </select>

                                    @if ($errors->has('pattern'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pattern') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ls_index') ? ' has-error' : '' }}">
                                <label for="ls_index" class="col-md-4 control-label">Ls index</label>

                                <div class="col-md-6">
                                    <input id="ls_index" type="text" class="form-control" name="ls_index" value="{{ old('ls_index') }}">

                                    @if ($errors->has('ls_index'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ls_index') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tube_type') ? ' has-error' : '' }}">
                                <label for="tube_type" class="col-md-4 control-label">Tube type</label>

                                <div class="col-md-6">
                                    <select id="tube_type" class="form-control" name="tube_type" required>
                                        <option value="0">TL</option>
                                        <option value="1">TT</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type" class="col-md-4 control-label">Type</label>

                                <div class="col-md-6">
                                    <select id="type" class="form-control" name="type" required>
                                        <option>Choose..</option>
                                        <option selected>Spoljna</option>
                                        <option>Protektirana</option>
                                        <option>Komplet</option>
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('action') ? ' has-error' : '' }}">
                                <label for="action" class="col-md-4 control-label">Action</label>

                                <div class="col-md-6">
                                    <select id="action" class="form-control" name="action" required>
                                        <option selected disabled>Choose..</option>
                                        <option value="1">Da</option>
                                        <option value="0" selected="">Ne</option>
                                    </select>

                                    @if ($errors->has('action'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('action') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('availability') ? ' has-error' : '' }}">
                                <label for="availability" class="col-md-4 control-label">Availability</label>

                                <div class="col-md-6">
                                    <select id="availability" class="form-control" name="availability" required>
                                        <option selected>Choose..</option>
                                        <option>Ima</option>
                                        <option>Nema</option>
                                        <option selected>Proveri</option>
                                    </select>

                                    @if ($errors->has('availability'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('availability') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label for="active" class="col-md-4 control-label">Active</label>
                                <div class="col-md-6">
                                    <input type="checkbox" value="1" name="active" checked id="active">

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Price</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="{{old('price')}}" name="price" id="price">

                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fav') ? ' has-error' : '' }}">
                                <label for="fav" class="col-md-4 control-label">Favourite</label>
                                <div class="col-md-6">
                                    <input type="checkbox" value="1" name="favourite" id="active">

                                    @if ($errors->has('fav'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fav') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
