@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Emails</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-primary pull-left" >Emails</a>
        </div>
    </div>

    <table class="table table-bordered" id="pattern-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Email</th>
            <th>Date</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#pattern-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-emails/',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'email', name: 'email'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    </script>
@endsection
