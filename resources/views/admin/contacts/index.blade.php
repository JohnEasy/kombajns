@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Contacts</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-primary pull-left" >Contacts</a>
        </div>
    </div>

    <table class="table table-bordered" id="pattern-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Content</th>
            <th>Created at</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#pattern-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-contacts/',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'content', name: 'content'},
                {data: 'created_at', name: 'created_at'},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    </script>
@endsection
