@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Create user</h3>
        </div>
        <form class="form-horizontal col-sm-offset-1" action="{{action('Admin\AdminController@store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="email" class="form-control" name="email" placeholder="Enter email" value="{{old('email')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{old('name')}}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <input type="password" class="form-control" name="password" placeholder="Enter password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repeat password" required>
                </div>
            </div>

            <div class="form-group">
                <div class="radio">
                    <label><input type="radio" name="active" value="1" {{old('active') == 1 ? "checked" : ''}}>Active</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="active" value="0" {{old('active') == 0 ? "checked" : ''}}>Not active</label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary pull-left">Submit</button>
                </div>
            </div>
        </form>
    </div>

@stop