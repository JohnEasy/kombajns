@extends('layouts.admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Patterns</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="/admin/pattern/create" class="btn btn-primary pull-left" >Create new pattern</a>
        </div>
    </div>

    <table class="table table-bordered" id="pattern-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
    <script>
        $('#pattern-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/fetch-patterns/',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    </script>
@endsection
