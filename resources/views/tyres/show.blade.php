@extends('layouts.main.master')

@section('content')
    <!-- Main content -->
    <div id="wrap-body" class="p-t-lg-30">
        <div class="container">
            <div class="wrap-body-inner">
                <!-- Breadcrumb-->
                <div class="hidden-xs">
                    <div class="row">

                        <div class="col-lg-6 pull-right">
                            <a href="javascript:history.back();" class="ht-btn ht-btn-default pull-right m-t-lg-0"><i class="fa fa-backward"></i>Nazad na pretragu</a>
                        </div>
                    </div>
                </div>
                <!-- Car detail -->
                <section class="m-t-lg-30 m-t-xs-0">
                    <div class="product_detail no-bg p-lg-0">
                        <!-- Car name -->
                        <h3 class="product-name color1-f">{{$tyre->displayDimensions($tyre->width, $tyre->height)}}{{$tyre->formatConstruction()}}{{$tyre->radius}} {{$tyre->manufacturer->name}} {{$tyre->pattern->name}} /
                            <span class="product-price color-red">{{number_format($tyre->price)}}  <i class="color-9 color1-9"> RSD + PDV </i></span>
                        </h3>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <!-- Car image gallery -->
                                <div class="product-img-lg bg-gray-f5 bg1-gray-15">
                                    <div class="image-zoom row m-t-lg-5 m-l-lg-ab-5 m-r-lg-ab-5">
                                        <div class="col-md-12 col-lg-12 p-lg-5">
                                            <a href="images/default.png">
                                                <img src="{{asset($tyre->pattern->image_path)}}"
                                                     alt="{{$tyre->pattern->name}}"
                                                     title="{{$tyre->pattern->name}}">
                                                <img src="{{asset($tyre->manufacturer->logo_path)}}"
                                                     title="{{$tyre->manufacturer->name}}"
                                                     alt="{{$tyre->manufacturer->name}}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Car description -->
                            <div class="col-md-8 col-lg-8 col-sm-8">
                                <ul class="product_para-1 p-lg-15 bg-gray-f5 bg1-gray-15">
                                    <li><span>Tip :</span>{{$tyre->type}} guma</li>
                                    <li><span>Dimenzija :</span>{{$tyre->displayDimensions($tyre->width, $tyre->height)}}{{$tyre->formatConstruction()}}{{$tyre->radius}}</li>
                                    <li><span>Proizvodjac :</span>{{$tyre->manufacturer->name}}</li>
                                    <li><span>Dezen :</span>{{$tyre->pattern->name}}</li>
                                    <li><span>Nosivost i tip :</span>{{$tyre->ls_index}} {{$tyre->formatTubeType()}}</li>
                                    <li><span>Garancija :</span>{{$tyre->format()}}</li>
                                    <li><span>Dostupnost :</span>{!! $tyre->checkAvailability() !!}</li>
                                    @if ($tyre->action == 1)
                                        <li style="color: red"><span style="color: red">Akcijska cena :</span>{{number_format($tyre->price)}} RSD + PDV</li>
                                    @else
                                        <li><strong><span>Cena :</span>{{number_format($tyre->price)}} RSD + PDV</strong></li>
                                    @endif
                                    <li style="text-align:justify;"><span>Opis :</span>{{$tyre->pattern->description}}</li>
                                </ul>
                                {{--<h4>Slične pretrage / tagovi:</h4>--}}

                                {{--@foreach($tyre->tags as $tag)--}}
                                    {{--<span class="">--}}
                                        {{--<a href="/gume-i-cene?tag={{$tag->name}}" class="btn btn-danger btn-xs">{{$tag->name}}</a>--}}
                                    {{--</span>--}}
                                {{--@endforeach--}}
                            </div>
                        </div>
                    </div>
                    <!-- Car description tabs -->
                    <div class="row m-t-lg-30 m-b-lg-50">
                        <div class="col-md-8">
                            <div class="m-b-lg-30">
                                <div class="heading-1"><h3>Traktorske gume: <i style="color: red" class="color-9 color1-9"> {{$tyre->manufacturer->name}}</i></h3></div>
                                <div class="m-b-lg-30 bg-gray-fa bg1-gray-2 p-lg-30 p-xs-15" style="text-align:justify;">
                                    <p class="color1-9">
                                        {!! nl2br($tyre->manufacturer->description)!!}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Dealer Infomation -->
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="heading-1">
                                <h3><i class="fa fa-info-circle"></i>Info i naruČivanje</h3>
                            </div>
                            <h4 class="p-t-lg-0"><a href="#">Vir Agrar d.o.o. Beograd</a></h4>
                            <div class="clearfix"></div>
                            <ul class="list-default m-t-lg-0">
                                <li><i class="fa fa-phone m-r-lg-10 icon"></i>062 404 222</li>
                                <li><i class="fa fa-envelope-o m-r-lg-10 icon"></i>viragrar@gmail.com</li>
                                <li><a href="http://www.viragrar.rs" target="_blank"><i class="fa fa-globe m-r-lg-10 icon"></i>http://www.viragrar.rs</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- NAJTRAŽENIJE: -->
                    <div class="product product-grid product-grid-2 car m-b-lg-15">
                        <div class="heading">
                            {{--<h3>NAJTRAŽENIJE:</h3>--}}
                        </div>
                        <div class="row">
                            {{--@foreach($favourites as $tyre)--}}

                                {{--<div class="col-sm-12 col-md-6 col-lg-4">--}}
                                    {{--<!-- Product item -->--}}
                                    {{--<div class="product-item hover-img">--}}
                                        {{--<a href="/kamionske-gume/{{$tyre->slug}}" class="product-img">--}}
                                            {{--<img src="{{asset($tyre->pattern->image_path)}}"--}}
                                                 {{--alt="{{$tyre->pattern->name}}"--}}
                                                 {{--title="{{$tyre->pattern->name}}">--}}
                                            {{--<img src="{{asset($tyre->manufacturer->logo_path)}}"--}}
                                                 {{--title="{{$tyre->manufacturer->name}}"--}}
                                                 {{--alt="{{$tyre->manufacturer->name}}">--}}
                                        {{--</a>--}}
                                        {{--<div class="product-caption">--}}
                                            {{--<h4 class="product-name">--}}
                                                {{--<a href="#">{{$tyre->displayDimensions($tyre->width, $tyre->height)}}R{{$tyre->radius}} / <b>{{$tyre->manufacturer->name}} {{$tyre->pattern->name}}</b>--}}
                                                {{--</a>--}}
                                                {{--<span class="f-18"> {{number_format($tyre->price)}}+pdv</span>--}}
                                            {{--</h4>--}}
                                        {{--</div>--}}
                                        {{--<ul class="absolute-caption">--}}
                                            {{--@foreach($tyre->tags as $tag)--}}
                                                {{--<li><i class="fa fa-tag"></i>{{$tag->name}}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection