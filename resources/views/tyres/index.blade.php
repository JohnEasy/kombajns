@extends('layouts.main.master')

@section('content')
    <!-- Banner -->
    <section class="block-sl">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="banner-item banner-2x no-bg color-inher">
                        <strong>
                            <h1 class="f-weight-300">TRAKTORSKE GUME</h1>
                            <h2>VIR AGRAR d.o.o. BEOGRAD</h2>

                            <a href="{{url('gume-i-cene')}}" class="ht-btn ht-btn-default ht-btn-2x m-t-lg-35">Gume & Cene</a>
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Search option-->
    <div class="search-1 m-t-sm-40">
        <div class="container">
            <div class="search-option p-lg-30 p-b-lg-15 p-b-sm-30 p-r-sm-45 p-xs-15">
                <div class="row">
                    <form method="get" action="{{'gume-i-cene'}}">
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 m-b-lg-15 p-r-lg-0 p-r-xs-15">
                                    <div class="">
                                        <div class="dropdown">
                                            <select name="width" class="form-control" title="width" id="width">
                                                <option selected value="" >Širina</option>
                                                @foreach($widths as $width)
                                                    <option value="{{$width}}" {{$width == request('width') ? 'selected' : ''}} class="width">{{$width}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 m-b-lg-15 p-r-lg-0 p-r-xs-15">
                                    <div class="">
                                        <div class="dropdown">
                                            <select name="height" class="form-control" title="height" id="height">
                                                <option selected value="">Visina</option>
                                                @foreach($heights as $height)
                                                    <option value="{{$height}}" {{$height == request('height') ? 'selected' : ''}} class="height">{{$height}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 m-b-lg-15 p-r-lg-0 p-r-xs-15">
                                    <div class="">
                                        <div class="dropdown">
                                            <select name="radius" class="form-control" title="radius" id="radius">
                                                <option selected value="">Prečnik</option>
                                                @foreach($radiuses as $radius)
                                                    <option value="{{$radius}}" {{$radius == request('radius') ? 'selected' : ''}} class="radius">{{$radius}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 m-b-lg-15 p-r-lg-0 p-r-xs-15">
                                    <div class="">
                                        <div class="dropdown">
                                            <select name="manufacturer" class="form-control" id="manufacturer">
                                                <option selected value="">Proizvođač</option>
                                                @foreach($manufacturers as $manufacturerId => $manufacturer)
                                                    <option value="{{$manufacturerId}}" {{$manufacturerId == request('manufacturer') ? 'selected' : ''}} class="manufacturer">{{$manufacturer}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-2 col-lg-2 p-r-sm-0 pull-right pull-left-xs" style="margin-top: -3%">
                            <button type="submit" class="ht-btn ht-btn-default m-t-lg-30 m-t-sm-10 pull-right pull-left-xs"><i class="fa fa-search"></i> Pretraži</button>
                        </div>
                        <span class="pull-right color-f" style="margin-right: 6%; margin-top: 5px" id="guma" hidden>
                            <i class="fa fa-spin color-f"></i>
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content-->
    <div id="wrap-body" class="p-t-lg-45">
        <div class="container">
            <div class="wrap-body-inner">
                <!-- Recent cars -->
                <div class="product product-grid product-grid-2 car m-t-lg-90 p-t-sm-35 m-b-lg-20">
                    <div class="heading">
                        <h3>NAJTRAŽENIJE:</h3>
                    </div>
                    <div class="row">
                        <div class="owl" data-items="5" data-itemsDesktop="2" data-itemsDesktopSmall="2" data-itemsTablet="2" data-itemsMobile="1" data-pag="false" data-buttons="true">
                            @foreach($favourites as $tyre)
                                <div class="col-lg-10">
                                    <!-- Product item -->
                                    <div class="product-item hover-img">
                                        <a href="/traktorske-gume/{{$tyre->slug}}" class="product-img">
                                            <img src="{{asset($tyre->pattern->image_path)}}"
                                                 alt="{{$tyre->pattern->name}}"
                                                 title="{{$tyre->pattern->name}}">
                                            <img src="{{asset($tyre->manufacturer->logo_path)}}"
                                                 title="{{$tyre->manufacturer->name}}"
                                                 alt="{{$tyre->manufacturer->name}}">
                                        </a>
                                        {{--<div class="product-caption">--}}
                                        <h6 class="product-name">
                                            <a  href="#">{{$tyre->displayDimensions($tyre->width, $tyre->height)}}{{$tyre->formatConstruction()}}{{$tyre->radius}} <br><b>{{$tyre->manufacturer->name}} {{$tyre->pattern->name}}
                                                </b>
                                            </a><br>
                                            <span style="color: red;"> {{number_format($tyre->price)}}+pdv</span>
                                        </h6>
                                        {{--</div>--}}
                                        {{--<ul class="absolute-caption">--}}
                                            {{--@foreach($tyre->tags as $tag)--}}
                                                {{--<li><i class="fa fa-tag"></i>{{$tag->name}}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ul>--}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <section class="m-t-lg-30 m-t-xs-0 m-b-lg-50">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 m-b-lg-50">
                                <div class="heading">
                                    <h3>NAŠA PONUDA:</h3>
                                </div>
                                <p>
                                <h4>Vir Agrar d.o.o. iz Beograda se bavi veleprodajom i online maloprodajom svih vrsta pneumatika.</h4>
                                <p>Pozivamo Vas da nas slobodno kontaktirate - zahvaljujuci dugogodišnjem iskustvu u ovom poslu, možemo Vam pomoci u izboru brenda, dezena i šara, preneti iskustva drugih korisnika, pronaci gume koje su baš Vama potrebne...</p>


                                <h3>POLJOPRIVREDNE GUME:</h3>
                                <p>traktorske gume / gume za kombajn / gume za prikolicu / gume za prikljucne mašine</p>
                                <p>radijalne gume za traktore i kombajne / dijagonalne gume / unutrašnje gume / pojasevi</p>
                                <p>Bridgestone / Firestone / OZKA / Petlas / BKT / Alliance / Galaxy / Mitas / Cultor</p>

                                <h3>INDUSTRIJSKE GUME:</h3>
                                <p>gume za radne mašine / gume za utovarivace i kombinirke / gume za dampere / gume za viljuškare / gume za skip i ULT</p>
                                <p>radijalne industrijske gume / dijagonalne gume / unutrašnje gume / pojasevi</p>
                                <p>Bridgestone / Firestone / OZKA / Petlas / BKT / Alliance / Galaxy / Mitas / Cultor</p>

                                <h3>TERETNE GUME :</h3>
                                <p>Kamionske gume / gume za prikolice / pogonske i vodece gume / pogonske gume za kipere i šticare / prednje kiperske gume / gume za lake kamione i dostavna vozila</p>

                                <p>Nove kamionske gume cene / protektirane gume / kineske gume cene / ruske gume cene / evropske proizvodaci</p>

                                <p>BRENDOVI: Bridgestone / Firestone / Dayton / Continental / Bandag / Protread / Mirage / Cordiant / West Lake / Linglong / Double Coin / Zeetex / Sailun / Aeoulus</p>

                                <h4>IMAMO PRAVU GUMU ZA VAŠ POSAO!!!</h4>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Banner -->
                <div class="banner-item banner-2x banner-bg-9 color-inher m-b-lg-50">
                    <h3 class="f-weight-300" style="color: #007a00;"><strong style="background-color: yellow">Najpovoljnije kamionske gume</strong></h3>

                    <a href="http://www.gumezakamion.com" target="_blank" class="ht-btn ht-btn-default"><strong>Poseti: gumezakamion.com</strong></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/custom.js')}}"></script>
@endsection