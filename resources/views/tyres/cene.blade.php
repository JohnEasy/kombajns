@extends('layouts.main.master')

@section('content')
    <!-- Main content -->
    <div id="wrap-body" class="p-t-lg-30">
        <div class="container">
            <div class="wrap-body-inner">
                <!-- Breadcrumb-->
                <div class="hidden-xs">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="ht-breadcrumb pull-left">
                                <li class="home-act"><a href="/"><i class="fa fa-home"></i></a></li>
                                <li class="home-act">
                                    <a href="#">Prikazano rezultata: {{$total}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Car list -->
                <section class="block-product m-t-lg-30 m-t-xs-0">
                    <div class="row">
                        <div class="col-sm-5 col-md-4 col-lg-3">
                            <!-- Search option -->
                            <form method="get" action="{{'gume-i-cene'}}">
                                <div class="search-option m-b-lg-50 p-lg-20">
                                    <div class="m-b-lg-15">

                                        <div class="">
                                            <select name="width" class="form-control" title="width" id="width">
                                                <option selected value="" >Širina</option>
                                                @foreach($widths as $width)
                                                    <option value="{{$width}}" {{$width == request('width') ? 'selected' : ''}} class="width">{{$width}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="m-b-lg-15">
                                        <div class="dropdown">
                                            <select name="height" class="form-control" title="height" id="height">
                                                <option selected value="">Visina</option>
                                                @foreach($heights as $height)
                                                    <option value="{{$height}}" {{$height == request('height') ? 'selected' : ''}} class="height">{{$height}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="m-b-lg-15">
                                        <div class="dropdown">
                                            <select name="radius" class="form-control" title="radius" id="radius">
                                                <option selected value="">Prečnik</option>
                                                @foreach($radiuses as $radius)
                                                    <option value="{{$radius}}" {{$radius == request('radius') ? 'selected' : ''}} class="radius">{{$radius}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="m-b-lg-15">
                                        <div class="dropdown">
                                            <select name="manufacturer" class="form-control" id="manufacturer">
                                                <option selected value="">Proizvođač</option>
                                                @foreach($manufacturers as $manufacturerId => $manufacturer)
                                                    <option value="{{$manufacturerId}}" {{$manufacturerId == request('manufacturer') ? 'selected' : ''}} class="manufacturer">{{$manufacturer}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="ht-btn ht-btn-default m-t-lg-30"><i class="fa fa-search"></i>PretraŽi</button>
                                    <span class="pull-right color-f" id="guma" hidden>
                                        <i class="fa fa-spin color-f" ></i>
                                    </span>

                                </div>
                            </form>
                            <div class="clearfix"></div>
                            <!-- Banner -->
                            <div class="banner-item banner-bg-4 banner-1x color-inher">
                                <h3 class="f-weight-300"><strong style="background-color:#9e9e9e">Najpovoljnije kamionske gume</strong></h3>
                                <a href="http://gumezakamion.com" target="_blank" class="ht-btn ht-btn-default">Poseti: gumezakamion.com</a>
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-8 col-lg-9">
                            <!-- Car -->
                            <div class="product product-list car">
                                <div class="clearfix"></div>
                                <!-- Product item -->
                                @foreach($tyres as $tyre)
                                    <div class="product-item hover-img">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-3 col-lg-3">
                                                {{--<a href="#" class="product-img"><img src="images/default.png" alt="image"></a>--}}
                                                <a href="/traktorske-gume/{{$tyre->slug}}">
                                                    <img src="{{asset($tyre->pattern->image_path)}}" class="img-rounded" alt="{{$tyre->pattern->name}}" title="{{$tyre->pattern->name}}" width="100%" >
                                                </a>
                                                <img src="{{asset($tyre->manufacturer->logo_path)}}" class="" alt="{{$tyre->manufacturer->name}}" title="{{$tyre->manufacturer->name}}" width="100%" >
                                            </div>
                                            <div class="col-sm-12 col-md-9 col-lg-9">
                                                <div class="product-caption">
                                                    <h4 class="product-name">
                                                        <a href="/traktorske-gume/{{$tyre->slug}}" class="f-18">{{$tyre->displayDimensions($tyre->width, $tyre->height)}}{{$tyre->formatConstruction()}}{{$tyre->radius}} {{$tyre->manufacturer->name}} {{$tyre->pattern->name}}</a>
                                                    </h4>
                                                    <div class="text-success">{{$tyre->ls_index}} {{$tyre->formatTubeType()}}</div>
                                                    <b class="product-price">
                                                        @if ($tyre->action == 1)
                                                            <span class="red"> AKCIJSKA CENA: {{number_format($tyre->price)}} RSD + PDV</span>
                                                        @else
                                                            <div>
                                                                CENA: {{number_format($tyre->price)}} RSD + PDV
                                                            </div>
                                                        @endif
                                                    </b>
                                                    <div class="product-txt m-t-lg-10">
                                                        {{$tyre->catTheCrap($tyre->pattern->description, 100)}}
                                                    </div>
                                                    <ul class="static-caption m-t-lg-20">
                                                        {{--@foreach($tyre->tags as $tag)--}}
                                                            {{--<li>--}}
                                                                {{--<i class="fa fa-tag"></i>--}}
                                                                {{--<span >--}}
                                                                {{--<a href="/gume-i-cene?tag={{$tag->name}}" style="color:{{request('tag') == $tag->name ? 'red' : ''}}">{{$tag->name}}</a>--}}
                                                            {{--</span>--}}
                                                            {{--</li>--}}
                                                        {{--@endforeach--}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                {{$tyres->appends([
                                    'width' => Request::get('width'),
                                    'height' => Request::get('height'),
                                    'radius' => Request::get('radius'),
                                    'manufacturer' => Request::get('manufacturer'),
                                    'tag' => Request::get('tag'),
                            ])->fragment('results')->links('vendor.pagination.default')}}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/custom.js')}}"></script>
@endsection