<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Traktorske gume, gume za poljoprivredu, industrijske gume i gume za kombajne. Veliki izbor proizvodaca. POVOLJNO. Garancija. Sigurna trgovina. Isporuka. 062/404-222 viragrar@gmail.com">

    <title>Traktorske gume - Vir Agrar d.o.o.</title>
    <link rel="icon" href="{{asset('favicon.ico')}}">
    <!-- JqueryUI -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.css')}}">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/boostrap/bootstrap.min.css')}}">
    <!-- Awesome font icons -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <!--magnific popup-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup/magnific-popup.css')}}" media="screen"/>
    <!-- Owlcoursel -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl-coursel/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl-coursel/owl.theme.css')}}">
    <!-- Main style -->
    <link  rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <!-- Padding / Margin -->
    <link  rel="stylesheet" type="text/css" href="{{asset('css/padd-mr.css')}}">
    <!-- dark version-->
    <link id="vers" rel="stylesheet" type="text/css" href="{{asset('css/light-version.css')}}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105218922-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-105218922-1');
    </script>
</head>
<body class="bg-3">
<!-- Preloader -->
<div class="preloader"><p></p></div>
<div id="wrap" class="color1-inher">
    <!-- Header-->
    <header id="wrap-header" class="color-inher">
        <div class="top-header">
        </div>
        <!-- Menu -->
        <div class="menu-bg">
            <div class="container">
                <div class="row">
                    <!-- Logo -->
                    <div class="col-md-3 col-lg-3">
                        <a href="/" class="logo"><img src="{{asset('images/logo.jpg')}}" alt="logo"></a>
                    </div>
                    <div class="col-md-9 col-lg-9">
                        <div class="hotline">
                            <span class="m-r-lg-10">Info i naručivanje:</span>
                            <i class="fa fa-phone"></i>062 404 222
                        </div>
                        <div class="clearfix"></div>
                        <!-- Menu -->
                        <div class="main-menu">
                            <div class="container1">
                                <nav class="navbar navbar-default menu">
                                    <div class="container1-fluid">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                            <ul class="nav navbar-nav">
                                                <li class="dropdown">
                                                    <a href="{{url('/')}}">Naslovna</a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{url('gume-i-cene')}}">Gume & Cene</a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{url('novosti')}}">Novosti</a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{url('instructions')}}">Kako kupiti?</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
@yield('content')
<!-- Footer-->
    <footer id="wrap-footer" class="bg-gray-1 color-inher">
        <!-- Footer top -->
        <div class="footer-top">
            <div class="container">
                <div class="bg-gray-1 p-l-r">
                    <div class="row">
                        <!-- Company info -->
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="heading-1">
                                <h3>Vir Agrar d.o.o. Beograd</h3>
                            </div>
                            <p>Preduzeće specijalizovano za prodaju guma za transport, poljoprivredu i industriju.</p>
                            <ul class="list-default">
                                <li><i class="fa fa-signal"></i>Vojvode Brane 47, Beograd, Srbija</li>
                                <li><i class="fa fa-phone"></i>+381 (0)62 404 222</li>
                                <li><i class="fa fa-envelope-o"></i>viragrar@gmail.com</li>
                                <li><i class="fa fa-globe"></i><a target="_blank" href="http://www.viragrar.rs">http://www.viragrar.rs</a></li>
                            </ul>
                        </div>
                        <!-- Newsletter -->
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="newsletter text-left">
                                <div class="heading-1">
                                    <h3>Mailing lista</h3>
                                </div>
                                <p>Prijavi se za našu mailing listu i dobijaj redovna obaveštenja o akcijama, popustima i novostima. </p>
                                <div id="status" hidden></div>
                                <form>
                                    <div class="form-group">
                                        <input type="email" class="form-control form-item" id="email" placeholder="Email">
                                    </div>
                                    <button type="submit" id="submit" class="ht-btn ht-btn-default">Prijavi se</button>
                                </form>
                            </div>
                        </div>
                        <!-- Quick link -->
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="heading-1">
                                <h3>Naši websajtovi</h3>
                            </div>
                            <ul class="list-default">
                                <li><a target="_blank" href="http://viragrar.rs/"><i class="fa fa-angle-right"></i>viragrar.rs</a></li>
                                <li><a target="_blank" href="http://gumezatraktor.com/"><i class="fa fa-angle-right"></i>gumezatraktor.com</a></li>
                                <li><a target="_blank" href="#"><i class="fa fa-angle-right"></i>gumezakamion.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer bottom -->
        <div class="footer-bt color-inher">
            <div class="container">
                <div class="bg-gray-0c p-l-r">
                    <div class="row">
                        <div class="col-md-6">
                            <p>© {{date('Y')}} Copyright by Vir Agrar d.o.o.</p>
                        </div>
                        <div class="col-md-6">
                            <ul class="social-icon list-inline pull-right">
                                <li><a target="_blank" href="https://www.facebook.com/Gume-Za-Kamion-Com-274417562994472/"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" href="https://www.linkedin.com/company/18020025/"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- jQuery -->
<script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
<!-- JqueryUI -->
<script src="{{asset('js/jquery/jquery-ui.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
<!--magnific popup-->
<script src="{{asset('js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<!-- Jquery.counterup -->
<script src="{{asset('js/jquery.counterup/waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.counterup/jquery.counterup.min.js')}}"></script>
<!-- Owl-coursel -->
<script src="{{asset('js/owl-coursel/owl.carousel.js')}}"></script>
<!-- Script -->
<script src="{{asset('js/script.js')}}"></script>

<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
    var sc_project=11399385;
    var sc_invisible=1;
    var sc_security="d7d2d6c7";
    var scJsHost = (("https:" == document.location.protocol) ?
        "https://secure." : "http://www.");
    document.write("<sc"+"ript type='text/javascript' src='" +
        scJsHost+
        "statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="website
statistics" href="http://statcounter.com/"
                                      target="_blank"><img class="statcounter"
                                                           src="//c.statcounter.com/11399385/0/d7d2d6c7/1/"
                                                           alt="website statistics"></a></div></noscript>

<!-- End of StatCounter Code for Default Guide -->
<script>
    $('#submit').on('click', function(e) {
        e.preventDefault();
        var email = $('#email').val();
        var status = $('#status');

        var request = $.ajax({

            method: "GET",
            url: "{{route('opt-in')}}",
            beforeSend: function() {
                $(status).text('sending...').show().css('color', 'orange');
            },
            data: {email: email},
            dataType: 'json'
        });

        request.done(function( msg ) {
            $(status).text(msg.message).show().css('color', 'green');
            $('#email').val('').removeAttr('style');
        });

        request.fail(function(jqXHR) {
            var error = JSON.parse(jqXHR.responseText);
            $(status).show().text(error.email).css('color', 'red');
            $('#email').css('border', '1px solid red');
        });
    });
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5a75d361d7591465c707592a/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

@yield('scripts')
</body>
</html>