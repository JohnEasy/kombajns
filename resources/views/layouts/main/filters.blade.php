<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
    <div class="post-preview">
        @include('partials.filters')

        @if (isset($tyres))
            <div class="container">
                <div class="row">
                    <p>Prikazano rezultata: {{$total}}</p>
                    <hr style="width: 100%; color: lightslategrey; height: 1px; background-color:darkslategrey;" />
                    @foreach($tyres as $tyre)
                        <ul class="media-list">
                            <li class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img src="{{asset($tyre->pattern->image_path)}}" class="img-rounded" alt="" width="200" >
                                        <img src="{{asset($tyre->manufacturer->logo_path)}}" class="" alt="" width="200" >
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$tyre->type}}</h4>
                                    <h6 class="media-heading">{{$tyre->displayDimensions($tyre->width, $tyre->height)}}
                                        R{{$tyre->radius}} {{$tyre->manufacturer->name}} {{$tyre->pattern->name}}</h6>
                                    <div>
                                        {{str_limit($tyre->pattern->description, 100)}}
                                    </div>
                                    <div class="">
                                        @if ($tyre->action == 1)
                                            <span style="color: red"> AKCIJSKA CENA: {{number_format($tyre->price)}} RSD + PDV</span>
                                        @else
                                            CENA: {{number_format($tyre->price)}} RSD + PDV
                                        @endif
                                    </div>

                                    <div class="">
                                        <a href="/detail-page/{{$tyre->slug}}" class="btn btn-warning">Detaljnije</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <hr style="width: 100%; color: lightslategrey; height: 1px; background-color:darkslategrey;" />

                    @endforeach
                </div>
            </div>
            {{$tyres->appends([
                'width' => Request::get('width'),
                'height' => Request::get('height'),
                'radius' => Request::get('radius'),
                'manufacturer' => Request::get('manufacturer'),
        ])->fragment('results')->links()}}
        @endif
    </div>
</div>
