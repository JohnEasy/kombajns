<header class="intro-header" style="background-image: url('{{asset('img/details.jpg')}}')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>Kontakti za informacije, proveru stanja i poručivanje</h1>
                    <h3 class="">
                        <span style="background-color: darkslategrey">viragrar@gmail.com
                        </span>
                    </h3>
                    <h3 class="">
                        <span style="background-color: darkslategrey; color: white">Tel: 062 404 222
                        </span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</header>
