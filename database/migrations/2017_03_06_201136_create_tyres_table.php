<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTyresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyres', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manufacturer_id')->unsigned();
            $table->integer('pattern_id')->unsigned();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->boolean('construction')->default(0);
            $table->string('radius')->nullable();
            $table->string('type');
            $table->integer('action')->defaut(null);
            $table->string('availability');
            $table->boolean('active')->default(0);
            $table->string('price');
            $table->string('ls_index');
            $table->boolean('tube_type')->default(0);
            $table->timestamps();

            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade');
            $table->foreign('pattern_id')->references('id')->on('patterns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyres');
    }
}
