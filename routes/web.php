<?php

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
});
//Clear Config cache:
Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('dashboard', 'AdminController@dashboard');

    // tyres
    Route::get('tyre/create', 'Tyre\TyreController@create');
    Route::get('tyre/', 'Tyre\TyreController@index');
    Route::get('tyre/{tyre}/edit', 'Tyre\TyreController@edit');
    Route::post('tyre/{tyre}/update', 'Tyre\TyreController@update');
    Route::post('tyre/create', 'Tyre\TyreController@store');
    Route::get('tyre/{tyre}/delete', 'Tyre\TyreController@destroy');
    Route::get('tyre/update-price', 'Tyre\TyreController@updatePrice')->name('updatePrice');

    // manufacturers
    Route::get('manufacturer/create', 'Manufacturer\ManufacturerController@create');
    Route::get('manufacturer/{manufacturer}/edit', 'Manufacturer\ManufacturerController@edit');
    Route::post('manufacturer/{manufacturer}/update', 'Manufacturer\ManufacturerController@update');
    Route::get('manufacturer', 'Manufacturer\ManufacturerController@index');
    Route::post('manufacturer/create', 'Manufacturer\ManufacturerController@store');
    Route::get('manufacturer/{manufacturer}/delete', 'Manufacturer\ManufacturerController@destroy');

    // patterns
    Route::get('pattern/create', 'Pattern\PatternController@create');
    Route::get('pattern/', 'Pattern\PatternController@index');
    Route::post('pattern/store', 'Pattern\PatternController@store');
    Route::get('pattern/{pattern}/edit', 'Pattern\PatternController@edit');
    Route::post('pattern/{pattern}/update', 'Pattern\PatternController@update');
    Route::get('pattern/{pattern}/delete', 'Pattern\PatternController@destroy');

    // news
    Route::get('news/', 'News\NewsController@index');
    Route::get('news/create', 'News\NewsController@create');
    Route::post('news/store', 'News\NewsController@store');
    Route::get('news/{news}/edit', 'News\NewsController@edit');
    Route::post('news/{news}/update', 'News\NewsController@update');
    Route::get('news/{news}/delete', 'News\NewsController@destroy');

    // emails
    Route::get('emails/', 'Email\EmailController@index');
    Route::get('emails/{email}/delete', 'Email\EmailController@destroy');

    //contacts
    Route::get('contacts', 'Contact\ContactController@index');

    Route::get('fetch-manufacturers', 'DataTableController@manufacturers');
    Route::get('fetch-patterns', 'DataTableController@patterns');
    Route::get('fetch-tyres', 'DataTableController@tyres');
    Route::get('fetch-news', 'DataTableController@news');
    Route::get('fetch-emails', 'DataTableController@emails');
    Route::get('fetch-contacts', 'DataTableController@contacts');

});

Route::get('/', 'TyreController@index');
Route::get('/novosti', function () {
    return view('tyres.news');
});
//Route::get('/slug', 'TyreController@slug');

// contact page
Route::get('/instructions', 'ContactController@index');
Route::post('/instructions', 'ContactController@store');

Route::get('/traktorske-gume/{tyre}', 'TyreController@show');
Route::get('/gume-i-cene', 'TyreController@cene');

Route::post('search', 'TyreController@search');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('opt-in', 'MailController@store')->name('opt-in');

Route::get('/novosti', 'NewsController@index');
Route::get('/novosti/{title}', 'NewsController@show');

Route::get('total-tires', 'TyreController@totalTires');

Route::get('locale/{lang?}', function ($lang=null){


    App::setlocale($lang);

    return view ('welcome');
});

